FROM ubuntu:mantic

SHELL ["/bin/bash", "-c"]

ENV TZ=Etc/UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get -yqq update; \
    apt-get -yqq install apt-utils; \
    apt-get -yqq update; \
    apt-get -yqq dist-upgrade --allow-downgrades --allow-remove-essential --allow-change-held-packages; \
    apt-get -yqq install curl build-essential git; \
    apt-get -yqq install python3 python3-dev python3-distutils python3-venv python3-poetry; \
    apt-get -yqq install golang-go nodejs npm pip; \
    apt-get -yqq install kicad; \
    apt-get -yqq install docker.io docker-compose-v2 docker-buildx; \
    apt-get -yqq install libnss3 libnspr4 libasound2;

RUN useradd -m builder

USER builder

RUN curl --proto '=https' --tlsv1.3 -sSf https://sh.rustup.rs | sh -s -- -y; \
    source "$HOME/.cargo/env"; \
    cargo install mdbook cargo-cache; \
    cargo cache -a -r all
